
#include "stdafx.h"
#include"../Engine/Game.h"

int APIENTRY _tWinMain(_In_     HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_     LPTSTR    lpCmdLine,
	_In_     int       nCmdShow)
{
	Game myGame;
	myGame.Run(hInstance, hPrevInstance, lpCmdLine, nCmdShow);
}