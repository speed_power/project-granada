#include "stdafx.h"
#include "Actor.h"
#include "Game.h"
#define MYFVF ( D3DFVF_XYZ | D3DFVF_DIFFUSE)
/*struct Vertex
{
	FLOAT x, y, z, rhw;
	DWORD color;
};
*/
Actor::Actor()
{
}
Actor::~Actor()
{
}

void Actor::SetMesher(Mesher*_theMesher)
{
	theMesher = _theMesher;
}

void Actor::Draw(LPDIRECT3DDEVICE9 dev, D3DXMATRIX _trasl, D3DXMATRIX _scale,
	D3DXMATRIX _rotation)
{

	dev->SetFVF(MYFVF);

	D3DXMATRIX trasl = _trasl;
	D3DXMATRIX scale = _scale;
	D3DXMATRIX rotation = _rotation;


	//Multiplico las matrices
	D3DXMATRIX matFinal;
	D3DXMatrixIdentity(&matFinal);
	matFinal = scale * rotation * trasl; //Primero se escala, despues se rota y despues se traslada.


										 //Le digo a la placa que la matriz de mundo es "mat"
	dev->SetTransform(D3DTS_WORLD, &matFinal);

	dev->SetFVF(MYFVF);
	dev->SetStreamSource(0, theMesher->GetVb(), 0, sizeof(Vertex));
	dev->SetIndices(theMesher->GetIb());
	dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);


	//Especificamos cual VB vamos a usar
	dev->SetStreamSource(0, theMesher->GetVb(), 0, sizeof(Vertex));

	//Especificamos indices
	dev->SetIndices(theMesher->GetIb());

	//dibujamos
	dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
}