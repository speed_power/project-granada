#include "stdafx.h"
#include <d3d9.h> //Busca el header de directx en los path
#include "Game.h"
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto

#define MYFVF ( D3DFVF_XYZRHW | D3DFVF_DIFFUSE)

/*
Game::Game()
{
}
Game::~Game()
{
}
*/
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

D3DXMATRIX GetViewMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = -pos.x;
	transMat._42 = -pos.y;
	transMat._43 = -pos.z;

	D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(-rot.z);
	rotZMat._12 = sin(-rot.z);
	rotZMat._21 = -sin(-rot.z);
	rotZMat._22 = cos(-rot.z);

	return transMat * rotZMat;
};

void Game::Run(_In_     HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_     LPTSTR    lpCmdLine,
	_In_     int       nCmdShow)
{
	//Creamos la clase de la ventana
	WNDCLASSEX wcex;

	//Iniciamos sus valores en 0
	ZeroMemory(&wcex, sizeof(WNDCLASSEX));


	wcex.cbSize = sizeof(WNDCLASSEX); //Tama�o en bytes
	wcex.style = CS_HREDRAW | CS_VREDRAW; //Estilo de la ventana
	wcex.lpfnWndProc = WndProc; //Funcion de manejo de mensajes de ventana
	wcex.hInstance = hInstance; //Numero de instancia
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW); //Cursor del mouse
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //Color de fondo
	wcex.lpszClassName = L"GameWindowClass"; //Nombre del tipo (clase) de ventana

											 //Registro mi tipo de ventana en windows
	RegisterClassEx(&wcex);

	//Creo la ventana, recibiendo el numero de ventana
	HWND hWnd = CreateWindowEx(0, //Flags extra de estilo
		L"GameWindowClass", //Nombre del tipo de ventana a crear
		L"Title", //Titulo de la barra
		WS_OVERLAPPEDWINDOW, //Flags de estilos
		0, //X
		0, //Y
		640, //Ancho
		480, //Alto
		NULL, //Ventana padre
		NULL, //Menu
		hInstance, //Numero de proceso
		NULL); //Flags de multi ventana

	ShowWindow(hWnd, nCmdShow); //Muestro la ventana
	UpdateWindow(hWnd); //La actualizo para que se vea

						//Me comunico con directx por una interfaz, aca la creo
	LPDIRECT3D9 d3d = Direct3DCreate9(D3D_SDK_VERSION);

	//Creo los parametros de los buffers de dibujado (pantalla)
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
	d3dpp.Windowed = true;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;

	//Creo la interfaz con la placa de video
	LPDIRECT3DDEVICE9 dev;
	d3d->CreateDevice(D3DADAPTER_DEFAULT, //Cual placa de video
		D3DDEVTYPE_HAL, //Soft o hard
		hWnd, //Ventana
		D3DCREATE_HARDWARE_VERTEXPROCESSING, //Proceso de vertices por soft o hard
		&d3dpp, //Los parametros de buffers
		&dev); //El device que se crea
	Mesher *theOnlyMesher = new Mesher(dev);

	Actor *engineObject1 = new Actor();
	engineObject1->SetMesher(theOnlyMesher);
	float object1PosZ = 0;
	float object1RotX = 0;

	Actor *engineObject2 = new Actor();
	engineObject2->SetMesher(theOnlyMesher);

	while (true)
	{
		MSG msg;

		//Saco un mensaje de la cola de mensajes si es que hay
		//sino continuo
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
		{
			break;
		}

		//Actualizar
		//camara
		D3DXMATRIX view = GetViewMatrix(
			D3DXVECTOR3(0, 0, 0),
			D3DXVECTOR3(0, 0, 0));
		D3DXMATRIX projection;
		D3DXMatrixPerspectiveFovLH(
			&projection,
			D3DXToRadian(60),
			(float)640 / 480, //ancho der la pantalla dividido por el alto
			0.0f, //Distancia minima de vision
			50); //Distancia maxima de vision

		dev->SetTransform(D3DTS_VIEW, &view);
		dev->SetTransform(D3DTS_PROJECTION, &projection);

		dev->Clear(0, NULL, D3DCLEAR_TARGET, /*D3DCOLOR_XRGB(255, 0, 0)*/ D3DCOLOR_ARGB(0, 100, 0, 100), 1.0f, 0);
		dev->BeginScene();
		//Dibujar
		//Marco la traslacion, la escala y la rotacion antes de enviarlo por parametro. Esto me permite crear actores diferentes.
		//primera figura:
		D3DXMATRIX _transl;
		D3DXMatrixIdentity(&_transl);
		_transl._41 = 0;	//Esto mueve en X (columna 4, fila 1)
		_transl._42 = 0;	//Esto mueve en Y (columna 4, fila 2)
		_transl._43 = object1PosZ;	//Esto mueve en Z (columna 4, fila 3)
		D3DXMATRIX _scale;
		D3DXMatrixIdentity(&_scale);
		_scale._11 = 1.0f;
		_scale._22 = 1.0f;
		_scale._33 = 1.0f;
		D3DXMATRIX _rotation;
		D3DXMatrixIdentity(&_rotation);
		_rotation._11 = cos(D3DXToRadian(object1RotX));
		_rotation._12 = sin(D3DXToRadian(object1RotX));
		_rotation._21 = -sin(D3DXToRadian(object1RotX));
		_rotation._22 = cos(D3DXToRadian(object1RotX));

		object1PosZ += 0.01;
		object1RotX += -0.50;
		engineObject1->Draw(dev, _transl, _scale, _rotation);
		

		//segunda figura
		D3DXMATRIX transl2;
		D3DXMatrixIdentity(&transl2);
		transl2._41 = 0;
		transl2._42 = 0;
		transl2._43 = 5;
		D3DXMATRIX scale2;
		D3DXMatrixIdentity(&scale2);
		scale2._11 = 1.0f;
		scale2._22 = 1.0f;
		scale2._33 = 1.0f;
		D3DXMATRIX rotation2;
		D3DXMatrixIdentity(&rotation2);
		rotation2._11 = cos(D3DXToRadian(0));
		rotation2._12 = sin(D3DXToRadian(0));
		rotation2._21 = -sin(D3DXToRadian(0));
		rotation2._22 = cos(D3DXToRadian(0));

		engineObject2->Draw(dev, transl2, scale2, rotation2);

		dev->EndScene();
		dev->Present(NULL, NULL, NULL, NULL);
	}

	dev->Release();
	d3d->Release();
}

//Manejo de mensajes por ventana
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY)
	{
		//Si destruyeron esta ventana (cerraron) le pido
		//a windows que cierre la app
		PostQuitMessage(0);
		return 0;
	}

	//Si no maneje el mensaje antes, hago el comportamiento por defecto
	return DefWindowProc(hWnd, message, wParam, lParam);
}