#pragma once
//#include "Game.h"
#include "Mesher.h"
#include "stdafx.h"
class Actor
{
	Mesher *theMesher;
public:
	Actor();
	~Actor();
	void SetMesher(Mesher* theMesher);
	void Draw(LPDIRECT3DDEVICE9 dev, D3DXMATRIX _trasl, D3DXMATRIX _scale,
		D3DXMATRIX _rotation);
};