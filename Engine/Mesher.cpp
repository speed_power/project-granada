#include "stdafx.h"
#include "Mesher.h"
#include"Vertex.h"
#define MYFVF ( D3DFVF_XYZRHW | D3DFVF_DIFFUSE)
Mesher::Mesher(LPDIRECT3DDEVICE9 dev)
{
	struct Vertex
	{
		FLOAT x, y, z, rhw;
		DWORD color;
	};
	Vertex vertexes[] =
	{
		{ -0.5f, +0.5f, 0.0f, D3DCOLOR_XRGB(0, 0, 255) },
		{ +0.5f, +0.5f, 0.0f, D3DCOLOR_XRGB(0,0,255) },
		{ -0.5f, -0.5f, 0.0f, D3DCOLOR_XRGB(255,0,0) },
		{ +0.5f, -0.5f, 0.0f, D3DCOLOR_XRGB(0,255,0) },
	};
	WORD indexes[] = { 0,3,2,   //Los indices de los vertices del primer triangulo
		0,1,3 }; //Los del segundo triangulo

	dev->CreateVertexBuffer(
		4 * sizeof(Vertex),
		D3DUSAGE_WRITEONLY,
		MYFVF,
		D3DPOOL_MANAGED,
		&vb,
		NULL);
	dev->CreateIndexBuffer(
		6 * sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&ib,
		NULL);
	VOID *data;
	vb->Lock(0, 0, &data, 0);
	memcpy(data, vertexes, 4 * sizeof(Vertex));
	vb->Unlock();
	ib->Lock(0, 0, &data, 0);
	memcpy(data, indexes, 6 * sizeof(WORD));
	ib->Unlock();
	//dev->SetRenderState(D3DRS_LIGHTING, false);

}
Mesher::~Mesher()
{
	ib->Release();
	vb->Release();
}
LPDIRECT3DVERTEXBUFFER9 Mesher::GetVb()
{
	return vb;
}
LPDIRECT3DINDEXBUFFER9 Mesher::GetIb()
{
	return ib;
}